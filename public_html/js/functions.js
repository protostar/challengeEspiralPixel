
/**
  * @desc  verifica a maior palavra de uma frase, imprime essa palavra e o n.º de caracteres
  * @return void
*/
function analisaFrase() {
    //obtém a frase a ser analisada
    var frase = document.getElementById("frase").innerHTML;
    
    //separa a frase por palavras e guarda num array
    var palavras = frase.split(" ");
   
    //receberá a(s) palavra(s) a imprimir
    var maiorPalavra = [];
    //receberá o nº de caracteres da maior palavra
    var nuCaracteres = 0;
    //receberá a frase a ser impressa com os resultados
    var result = null;
    //índice para utilizar no ciclo for
    var i = null;
    
    //obtém o número de caracteres da maior palavra
    for(i = 0; i < palavras.length; i++){
        if(palavras[i].length > nuCaracteres){
            nuCaracteres = palavras[i].length;
        }
    }

    //obtém a maior ou maiores palavras, caso seja mais do que uma.
    for(i = 0; i < palavras.length; i++){
        if(palavras[i].length === nuCaracteres){
            maiorPalavra.push(palavras[i]);
        }
    }
    
    //mostra os resultados conforme o número de palavras que existe a imprimir.
    if(maiorPalavra.length > 1) {
        result = 'As maiores palavras da frase são <span id="frase" class="font-italic text-info">\n\
                     ' + maiorPalavra.join() + '</span> e estas contêm <span id="frase" class="font-italic text-info">\n\
                     ' + nuCaracteres + '</span> caracteres.';
    } else {
        result = 'A maior palavra da frase é <span id="frase" class="font-italic text-info">\n\
                     ' + maiorPalavra.join() + '</span> e contem <span id="frase" class="font-italic text-info">\n\
                     ' + nuCaracteres + '</span> caracteres.'; 
    }
    
    //Imprime o resultado.
    document.getElementById("maiorPalavra").innerHTML = result;
}
